import stocks from '../../data/stocks'

const state = {
  stocks:[]
}

const mutations = {
  'SET_STOCKS' (state2, stocks){
    state.stocks = stocks
  },
  'RND_STOCKS' (state2){
    state.stocks.forEach(stock=>{
      stock.price = Math.round(stock.price * (1 + Math.random() - 0.5));
    })
  },
  'RND_TAVAROM' (state2){
    state.stocks.forEach(stock=>{
      var rand = Math.random()

    stock.priceNew = Math.round(stock.price + (rand * (stock.rang || 0) * 2 ) - (stock.rang || 0));
    //stock.price = Math.round(stock.price * (1 + Math.random() - 0.5));
  })
  },

}

const actions = {
  buyStock: ({commit}, order) => {
    commit('BUY_STOCKS', order);
  },
  initStocks: ({commit}) => {
    commit('SET_STOCKS', stocks);
  },
  randomizeStocks: ({commit}) => {
    commit('RND_STOCKS');
  },
  tavaromStocks: ({commit}) => {
    commit('RND_TAVAROM');
  },



}

const getters = {
  stocks: state =>{
    return state.stocks;
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}