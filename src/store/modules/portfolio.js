const state = {
  funds: 2000,
  stocks: [],
  lastChanging:'سبد شما خالیست',

}

const mutations = {
  'BUY_STOCKS' (state, {stockId, quantity, stockPrice}) {
    const record = state.stocks.find(element => element.id == stockId);
    if(record){
      record.quantity += quantity
    }else{
      state.stocks.push({
        id: stockId,
        quantity: quantity
      })
    }
    console.log(state)
    state.lastChanging=' شما تعداد ' + quantity +'  عدد از محصول ' + state.name +' اضاف کردی '
    state.funds -= stockPrice * quantity
  },
  'SELL_STOCKS' (state, {stockId, quantity, stockPrice}){
    const record = state.stocks.find(element => element.id == stockId);
    if(record.quantity > quantity){
      record.quantity -= quantity
    }else{
      state.stocks.splice(state.stocks.indexOf(record), 1)
    }

    state.lastChanging='شما تعداد' + quantity +'از محصول' + stockId +'فروختید'
    state.funds += stockPrice * quantity
  },
  'EMPTY_STOCKS' (state){
    state.stocks = []
    state.funds = 2000

  },
  'ABBAS_CLEAR_STORE'(){
    state.stocks=[]
    state.funds=2000
  }
}

const actions = {
  sellStock: ({commit}, order) => {
    commit('SELL_STOCKS', order)
  },
  emptyStock: ({commit}, order) => {
    commit('EMPTY_STOCKS', order)
  },
  abbasClearStore: ({commit}, ) => {
    console.log('abbasClearStore')
    commit( 'ABBAS_CLEAR_STORE')
  // commit('EMPTY_STOCKS', order)
},
}

const getters = {
  stockPorfolio(state, getters){
    return state.stocks.map(stock=>{
        const record = getters.stocks.find(element => element.id == stock.id);
        return {
          id: stock.id,
          quantity: stock.quantity,
          name: record.name,
          price: record.price
        }
      }
    )
  },
  myLastChange(state){
return state.lastChanging
  },
    tedad(state){

    return state.stocks.length
    },
  funds(state){
    return state.funds
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}